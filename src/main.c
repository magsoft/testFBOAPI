
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include <X11/X.h>
#include <X11/Xlib.h>

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>

#include <vlc/vlc.h>


Display                 *dpy;
Window                  root;
GLint                   att[] = { GLX_RGBA, GLX_DEPTH_SIZE, 24, GLX_DOUBLEBUFFER, None };
XVisualInfo             *vi;
Colormap                cmap;
XSetWindowAttributes    swa;
Window                  win;
GLXContext              glc;
XWindowAttributes       gwa;
XEvent                  xev;

GLuint texture;


void DrawAQuad() {
    glClearColor(1.0, 1.0, 1.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-1., 1., -1., 1., 1., 20.);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    gluLookAt(0., 0., 10., 0., 0., 0., 0., 1., 0.);

    glBindTexture(GL_TEXTURE_2D, texture);

    glBegin(GL_QUADS);
        glVertex3f(-.75, -.75, 0.); glTexCoord2f(1, 0);
        glVertex3f( .75, -.75, 0.); glTexCoord2f(1, 1);
        glVertex3f( .75,  .75, 0.); glTexCoord2f(0, 1);
        glVertex3f(-.75,  .75, 0.); glTexCoord2f(0, 0);
    glEnd();
}


void libVLCLog(void *data, int level, const libvlc_log_t *ctx,
         const char *fmt, va_list args)
{
    vprintf(fmt, args);
    printf("\n");
}


int main(int argc, char *argv[])
{
    XInitThreads ();

    dpy = XOpenDisplay(NULL);

    if (dpy == NULL)
    {
        printf("Cannot connect to X server\n");
        exit(1);
    }

    root = DefaultRootWindow(dpy);

    vi = glXChooseVisual(dpy, 0, att);

    if (vi == NULL)
    {
        printf("No appropriate visual found\n");
        exit(1);
    }
    else
        printf("Visual %p selected\n", (void *)vi->visualid); /* %p creates hexadecimal output like in glxinfo */


    cmap = XCreateColormap(dpy, root, vi->visual, AllocNone);

    swa.colormap = cmap;
    swa.event_mask = ExposureMask | KeyPressMask;

    win = XCreateWindow(dpy, root, 0, 0, 600, 600, 0, vi->depth, InputOutput, vi->visual, CWColormap | CWEventMask, &swa);

    XMapWindow(dpy, win);
    XStoreName(dpy, win, "VERY SIMPLE APPLICATION");

    glc = glXCreateContext(dpy, vi, NULL, GL_TRUE);
    glXMakeCurrent(dpy, win, glc);

    glewInit();

    glEnable(GL_DEPTH_TEST);

    /* Generate the texture to draw in */
    glEnable(GL_TEXTURE_2D);
    glActiveTexture(GL_TEXTURE0);
    glClientActiveTexture(GL_TEXTURE0);

    glGenTextures(1, &texture);
    printf("Gen Texture Id: %d\n", texture);


    /* LibVLC */
    libvlc_instance_t *inst;
    libvlc_media_player_t *mp;

    const char *args[] = {"--vout", "gl"};

    inst = libvlc_new(sizeof(args) / sizeof(*args), args);
    libvlc_log_set(inst, libVLCLog, NULL);

    /* Create a new item */
    libvlc_media_t *m = libvlc_media_new_path(inst, argv[1]);

    /* Create a media player playing environement */
    mp = libvlc_media_player_new_from_media(m);

    libvlc_video_set_glx_opengl_context(mp, dpy, glc, texture);

    libvlc_media_player_play(mp);

    glViewport(0, 0, gwa.width, gwa.height);

    while (1) {
        XNextEvent(dpy, &xev);

        if(xev.type == Expose) {
                XGetWindowAttributes(dpy, win, &gwa);
                glViewport(0, 0, gwa.width, gwa.height);
                while(1)
                {
                    DrawAQuad();
                    glXSwapBuffers(dpy, win);
                }
        }

        else if(xev.type == KeyPress) {
                glXMakeCurrent(dpy, None, NULL);
                glXDestroyContext(dpy, glc);
                XDestroyWindow(dpy, win);
                XCloseDisplay(dpy);
                exit(0);
        }
    }
}
